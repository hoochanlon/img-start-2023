# img-start-2023

image hosting server by GitLab

## git 下载

指定git clone位置，文件夹必须是空的，所以文件夹后面加入项目名就好了。

```
git clone https://github.com/hoochanlon/hoochanlon.git /Users/chanlonhoo/Pictures/asd
```

git下载项目指定文件，由[DownGit](https://minhaskamal.github.io/DownGit/#/home)提供支持


粘贴到 SSH Key

```shell
ssh-keygen -t rsa -b 4096 -C \
"youmail@outlook.com" \
&&  pbcopy <  ~/.ssh/id_rsa.pub
```

测试SSH连接

```shell
ssh -T git@gitlab.com
```

正常上传

```
git add . && git commit -m "upload" && git push
```

强制推送push与强制拉取pull

```
git push -f
```

```
git pull
```

解除脚本app门禁运行限制，仅限非正常运行的“损坏”程序执行。

```
sudo xattr -d com.apple.quarantine /Users/chanlonhoo/Pictures/img-start-2023/zOneGitpush.app
```
